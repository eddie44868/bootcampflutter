import 'dart:io';

void main(List<String> arguments) {
  //soal 1
  // var isThisY;
  // print("Apakah ingin menginstall aplikasi ini? ");
  // String jawab = stdin.readLineSync();
  // String yaa = "y";
  //     (yaa==jawab)? print("Anda akan menginstall aplikasi dart") : print("aborted");
  
  //soal 2
  print("Nama : ");
  String nama = stdin.readLineSync();
  print("Peran : ");
  String peran = stdin.readLineSync();

  if (nama!=null) {
    if (peran=='penyihir') {
      print("Selamat datang di Dunia Werewolf,"+nama);
      print("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
    } 
    else if(peran=='guard'){
      print("Selamat datang di Dunia Werewolf,"+nama);
      print("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }
    else if(peran=='werewolf'){
      print("Selamat datang di Dunia Werewolf,"+nama);
      print("Halo Werewolf "+nama+", kamu akan memakan mangsa setiap malam!");
    }
    else if(peran==''){
      print("Selamat datang di Dunia Werewolf,"+nama);
      print("Halo "+nama+", Pilih peranmu untuk memulai game!");
    }
    else {
      print("Peran yang dipilih tidak sesuai");
    }
  } else {
    print("Nama harus diisi!");
  }
  
  // soal 3
  // print("Hari : ");
  // String hari = stdin.readLineSync();
  // switch (hari) {
  //   case "senin": {
  //     print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
  //     break;
  //   }
  //   case "selasa": {
  //     print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
  //     break;
  //   }
  //   case "rabu": {
  //     print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
  //     break;
  //   }
  //   case "kamis": {
  //     print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
  //     break;
  //   }
  //   case "jumat": {
  //     print("Hidup tak selamanya tentang pacar.");
  //     break;
  //   }
  //   case "sabtu": {
  //     print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
  //     break;
  //   }
  //   case "minggu": {
  //     print("Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
  //     break;
  //   }

  //   default: {
  //     print("Tidak ada hari yang dipilih");
  //     break;
  //   }
  // }
  
  //soal 4
  // var tgl=29;
  // var bulan=2;
  // var tahun=2000;
  //  switch (bulan) {
  //   case 1: {
  //     if (tgl>=1 && tgl<=31) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Januari $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 2: {
  //     if (tgl>=1 && tgl<=28) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Februari $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else if (tgl==29) {
  //       if (tahun%4==0) {
  //         print("$tgl Februari $tahun");
  //       } else {
  //         print('Tanggal tidak sesuai');
  //       }
  //     }
  //      else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 3: {
  //     if (tgl>=1 && tgl<=31) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Maret $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 4: {
  //     if (tgl>=1 && tgl<=30) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl April $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 5: {
  //     if (tgl>=1 && tgl<=31) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Mei $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 6: {
  //     if (tgl>=1 && tgl<=30) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Juni $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 7: {
  //     if (tgl>=1 && tgl<=31) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Juli $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 8: {
  //     if (tgl>=1 && tgl<=31) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Agustus $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 9: {
  //     if (tgl>=1 && tgl<=30) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl September $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 10: {
  //     if (tgl>=1 && tgl<=31) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Oktober $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 11: {
  //     if (tgl>=1 && tgl<=30) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl November $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   case 12: {
  //     if (tgl>=1 && tgl<=31) {
  //       if (tahun>=1900 && tahun<=2200) {
  //         print("$tgl Desember $tahun");
  //       } else {
  //         print('Tahun tidak sesuai');
  //       }
  //     } else {
  //       print('Tanggal tidak sesuai');
  //     }
  //     break;
  //   }
  //   default: {
  //     print("Format tanggal tidak sesuai");
  //     break;
  //   }
  //  }

}
