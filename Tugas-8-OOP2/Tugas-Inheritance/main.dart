import 'attack_titan.dart';
import 'armor_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

main(List<String> args) {
  Attack a = Attack();
  Armor ar = Armor();
  Beast b = Beast();
  Human h = Human();

  a.powerPoint = 8;
  ar.powerPoint = 9;
  b.powerPoint = 7;
  h.powerPoint = 6;

  print("Power point Attack Titan : ${a.powerPoint}");
  print("Power point Armor Titan : ${ar.powerPoint}");
  print("Power point Beast Titan : ${b.powerPoint}");
  print("Power point Human : ${h.powerPoint}");
  print("");
  print("Sifat dari Attack Titan :"+a.punch());
  print("Sifat dari Armor Titan :"+ar.terjang());
  print("Sifat dari Beast Titan :"+b.lempar());
  print("Sifat dari Human :"+h.killAlltitan());
}