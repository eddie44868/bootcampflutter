import 'bangun_datar.dart';

class Persegi extends BangunDatar{
  double a = 0;

  Persegi (double a){
    this.a =a;
  }

  double Luas(){
    return a*a;
  }
  double Keliling(){
    return 4*a;
  }
}