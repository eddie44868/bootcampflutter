import 'bangun_datar.dart';

class Lingkaran extends BangunDatar{
  double a = 0;

  Lingkaran (double a){
    this.a =a;
  }

  double Luas(){
    return 22/7*a*a;
  }
  double Keliling(){
    return 2*3.14*a;
  }
}