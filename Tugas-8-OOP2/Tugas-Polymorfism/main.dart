import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

main(List<String> args) {
  BangunDatar bangun = new BangunDatar();
  Lingkaran lingkaran = new Lingkaran(7);
  Persegi kotak = new Persegi(4);
  Segitiga tiga = new Segitiga(10, 6, 7);

  bangun.convert();
  print("Luas & keliling lingkaran : ${lingkaran.Luas()} dan ${lingkaran.Keliling()}");
  print("Luas & keliling persegi : ${kotak.Luas()} dan ${kotak.Keliling()}");
  print("Luas & keliling segitiga : ${tiga.Luas()} dan ${tiga.Keliling()}");
}