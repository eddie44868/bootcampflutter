import 'bangun_datar.dart';

class Segitiga extends BangunDatar{
  double a = 0;
  double b = 0;
  double c = 0;

  Segitiga (double a, double b, double c){
    this.a =a;
    this.b = b;
    this.c = c;
  }

  double Luas(){
    return 0.5*a*b;
  }
  double Keliling(){
    return a+b+c;
  }
}