import 'dart:io';

Future delayedPrint(int detik, String message){
  final duration = Duration(seconds: 0);
  return Future.delayed(duration).then((value) => message);
}

main(List<String> args) {
  stdout.write('Life ');
  delayedPrint(2, "never flat").then((status){
    print(status);
  });
  stdout.write('is ');
}