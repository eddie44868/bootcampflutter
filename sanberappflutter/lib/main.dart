import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Quiz3/MainApp.dart';
import 'package:sanberappflutter/Tugas/Quiz3/LoginScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainApp(),
    );
  }
}

