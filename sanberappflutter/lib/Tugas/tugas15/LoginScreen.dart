import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/tugas15/HomeScreen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 60),
            Center(
              child: Text.rich(
                TextSpan(
                  text: 'Sanber Flutter',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.blue[300]),
                ),
                style: TextStyle(fontSize: 37),
              ),
            ),
            SizedBox(height: 30),
            Center(
              child: Image.asset(
                'assets/img/flutter.png',
                scale: 1.3,
              ),
            ),
            SizedBox(height: 30),
            TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.blue),
                ),
                hintText: 'Username',
              ),
            ),
            SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.blue),
                ),
                hintText: 'Password',
              ),
            ),
            SizedBox(height: 20),
            Center(
              child: Text.rich(
                TextSpan(
                  text: 'Forgot Password?',
                  style: TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.blue[300]),
                ),
                style: TextStyle(fontSize: 20),
              ),
            ),
            SizedBox(height: 20),
            SizedBox(
              height: 40,
              width: 1200,
              child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(primary: Colors.lightBlueAccent),
                  child: Text(
                    'Login',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()));
                  }),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text.rich(
                    TextSpan(
                      text: "Doesn't have account?",
                      style: TextStyle(
                          fontWeight: FontWeight.normal, color: Colors.black),
                    ),
                    style: TextStyle(fontSize: 20),
                  ),
                  Text.rich(
                    TextSpan(
                      text: "Sign In",
                      style: TextStyle(
                          fontWeight: FontWeight.normal, color: Colors.blue[300]),
                    ),
                    style: TextStyle(fontSize: 20),
                  ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
