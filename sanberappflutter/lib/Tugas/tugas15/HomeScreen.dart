import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/tugas15/BottomBar.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 37),
            Text.rich(
              TextSpan(
                  text: 'Welcome,',
                  style: TextStyle(fontWeight: FontWeight.bold, color:  Colors.blue[300]),
                  children: [
                    TextSpan(
                      text: ' Eddie',
                      style: TextStyle(fontWeight: FontWeight.normal, color:  Colors.blue[700]),
                    ),
                  ]),
              style: TextStyle(fontSize: 50),
            ),
            SizedBox(height: 30),
            TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 18),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                hintText: 'Search',
              ),
            ),
            SizedBox(height: 60),
            Text(
              'Recommended Places',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            SizedBox(height: 10),
            SizedBox(
              height: 210,
              child: GridView.count(
              crossAxisCount: 2,
              childAspectRatio: 1.4,
              crossAxisSpacing: 10,
              children: [
                for (var country in countries)
                  Image.asset('assets/img/$country.png')
              ],
            ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomBar(),
    );
  }
}

final countries = ['Monas', 'Roma', 'Berlin', 'Tokyo'];