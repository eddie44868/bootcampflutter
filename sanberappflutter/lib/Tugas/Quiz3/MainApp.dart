import 'package:flutter/material.dart';
import 'package:sanberappflutter/Tugas/Quiz3/HomeScreen.dart';
import 'package:sanberappflutter/Tugas/Quiz3/ChatScreen.dart';




class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  int _selectedIndex = 0;
  final _layoutPage =[
   HomeScreen(),
   ChatScreen(),
  ];
  void _onTabItem(int index){
    setState((){
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(  
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home")
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.chat),
            title: Text("Chat")
          ),
         
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}